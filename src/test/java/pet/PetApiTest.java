package pet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import dto.PetStucture;
import io.restassured.response.Response;
import module.ApiClientModule;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import services.PetApi;

import static org.hamcrest.Matchers.equalTo;

public class PetApiTest {

    private Injector injector = Guice.createInjector(new ApiClientModule());
    private PetApi petApi = injector.getInstance(PetApi.class);

    private static final Integer id = 9999;
    private static final String name = "Белла";
    private static final String status = "Cat";

    @Test
    @DisplayName("Создание питомца через апи")
    public void createPetTest() {
        //1 Создать и заполнить структуру питомца данными
        //2 Создать питомца через POST запрос
        //3 Проверить статус код запроса - 200
        //4 Проверить что id ответа совпадает с id запроса
        //5 Проверить что name ответа совпадает с name запроса
        //6 Проверить что status ответа совпадает с status запроса

        Response response;

        PetStucture petStucture = new PetStucture();
        petStucture.setId(id);
        petStucture.setName(name);
        petStucture.setStatus(status);

        response = petApi.createPet(petStucture);

        response
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("id", equalTo(id))
                .body("name", equalTo(name))
                .body("status", equalTo(status));
    }

    @Test
    @DisplayName("Получение питомца по id")
    public void getPetTest() {
        //1 Запросить питомца через GET запрос передав id
        //2 Проверить статус код ответа - 200
        //3 Проверить что id ответа совпадает с id созданного питомца
        //4 Проверить что name ответа совпадает с name созданного питомца
        //5 Проверить что status ответа совпадает с status созданного питомца

        Response response;

        response = petApi.getPet(id.toString());
        response
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body("id", equalTo(id))
                .body("name", equalTo(name))
                .body("status", equalTo(status));
    }

}
