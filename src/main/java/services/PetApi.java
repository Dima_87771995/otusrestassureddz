package services;

import dto.PetStucture;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class PetApi {
    private RequestSpecification spec;
    private static final String PET = "/pet";

    public PetApi() {
        spec = given()
                .baseUri(System.getProperty("api.base.url"))
                .contentType(ContentType.JSON);
    }

    public Response createPet(PetStucture pet) {
        return given(spec)
                .with()
                .body(pet)
                .log().all()
                .when()
                .post(PET);
    }

    public Response getPet(String id) {
        return given(spec)
                .with()
                .log().all()
                .when()
                .get(PET + String.format("/%s", id));
    }
}
