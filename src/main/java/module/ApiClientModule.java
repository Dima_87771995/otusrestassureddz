package module;

import com.google.inject.AbstractModule;
import services.PetApi;

public class ApiClientModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PetApi.class).asEagerSingleton();
    }

}
